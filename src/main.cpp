#include "main.hpp"

int main()
{
	
	
	std::signal(SIGINT, signalHandler);
	//track previous time
	double previousTime = 0;
	//track history
	std::vector<std::string> history;
	
	//loop for command prompt
	while(true)
	{
		//input string from command line
		std::string input = "";
		
		//prompt and get input
		//std::cout << "[cmd]: ";
		printPrompt();
		getline(std::cin, input);
		
		if(input == "")
			continue;
			
		//check if we want to quit
		if(input == "quit")
			break;
		
		//char array for converting string to char	
		char inputArr[input.length() + 1];
	
		//char pointer array for command arguments
		int len = getNumArgs(input) + 1;
		char* args[len];
		strcpy(inputArr, input.c_str());
		
		
		//set first pointer in array and start splitting char array by space
		char** nextArg = args;
		char* cmd = strtok(inputArr, " ");
		
		//put commands in args array folled by null terminater
		while(cmd != NULL)
		{
			*nextArg++ = cmd;
			cmd = strtok(NULL, " ");
		}
		*nextArg = NULL;
		
		//execute the command, returns time it took for execution (if execvp command)
		double execute = parseCommand(args, len, history, previousTime);
		
		//check if it was an execvp command or not, if so set it to previous time
		if(execute != -1)
			previousTime = execute;
		
		
		//insert the command into our history
		history.insert(history.end(), input);
	}
	return 0;
}

void pipeCommand(char* args[], int len, int pipes)
{
	const int PIPE_READ_END = 0;
	const int PIPE_WRITE_END = 1;
	const int STDIN = 0;
	const int STDOUT = 1;
	
	int savedSTDOUT = dup(STDOUT);
	int savedSTDIN = dup(STDIN);
	int locs[pipes];
	int pids[pipes + 1];
	
	pipe(pids);
	setPipeLocations(args, pipes, locs);
	
	int pid = fork();
	
	if(pid == 0)
	{
		dup2(pids[PIPE_WRITE_END], STDOUT);
		
		int pipeLocation = locs[0];
		
		char* pipeArgs[pipeLocation + 1];
		
		for(int i = 0; i < pipeLocation; i++)
		{
			pipeArgs[i] = args[i];
		}
		pipeArgs[pipeLocation] = NULL;
		
		//std::cout << "Doing First Process" << std::endl;
		execvp(pipeArgs[0], pipeArgs);
		std::cout << "First Process Failed" << std::endl;
	}
	
	int pid2 = fork();
	
	if(pid2 == 0)
	{
		dup2(pids[PIPE_READ_END], STDIN);
		
		close(pids[PIPE_WRITE_END]);
		
		int pipeLocation = locs[0];
		
		char* pipeArgs[len - pipeLocation];
		
		for(int i = 0; i < len; i++)
		{
			pipeArgs[i] = args[pipeLocation + 1 + i];
		}
		pipeArgs[len-pipeLocation-1] = NULL;
		
		//std::cout << "Doing Second Process" << std::endl;
		execvp(pipeArgs[0], pipeArgs);
		std::cout << "Second Process Failed" << std::endl;
	}
	
	int status;
	waitpid(pid, &status, 0);
	
	close(pids[PIPE_WRITE_END]);
	close(pids[PIPE_READ_END]);
	
	waitpid(pid2, &status, 0);
	
	dup2(savedSTDOUT, STDOUT);
	dup2(savedSTDIN, STDIN);
}

void signalHandler(int signum)
{
	
}

int numPipes(char* args[])
{
	int count = 0;
	int pipes = 0;
	while(args[count] != NULL)
	{
		if(*args[count++] == '|')
			pipes++;
	}
	
	return pipes;
}

void setPipeLocations(char* args[], int numPipes, int* locs)
{	
	//std::cout << "start location search" << std::endl;
	int count = 0;
	int index = 0;
	while(args[count] != NULL)
	{
		//std::cout << "Testing index: " << count << std::endl;
		if(*args[count] == '|')
			locs[index++] = count;
			
		count++;
	}
	
	//std::cout<< "Returning locations" << std::endl;
}

void printPrompt()
{
	char cwd[1024];
	
	if(getcwd(cwd, sizeof(cwd)) != NULL)
		fprintf(stdout, "[%s]: ", cwd);
	
}


//This is a recursive method that processes the command and returns the time spent executing
double parseCommand(char* args[], int len, std::vector<std::string> &history, double &previousTime)
{
	//check first if theres any pipes
	int pipes = numPipes(args);	

	if(pipes > 0)
	{
		pipeCommand(args, len, pipes);
	}
	//check if it was a ptime command
	else if(strcmp(args[0], "ptime") == 0)
	{
		//if so print previous time
		std::cout << std::setprecision(4) << "Time Spent Executing Child Process: " <<
			previousTime << " seconds" << std::endl;
	}
	//check if it was a history command
	else if(strcmp(args[0], "history") == 0)
	{
		//if so print history
		printHistory(history);
	}
	//check if it was a re-do command
	else if(strcmp(args[0], "^") == 0)
	{
		//if so convert history number to execute to an int
		std::stringstream str;
		
		str << args[1];
		
		int index;
		str >> index;
		
		//check to make sure the history command they want to execute is in the list
		if(index - 1 >= history.size())
			std::cout << "Option out of Range" << std::endl;
		else
		{
			//if it is, get that command from history, 
			//convert it to a char array, and tokenize it (like we did the first time)
			std::string command = history.at(index - 1);
			
			char historyArr[command.length() + 1];

			int newLength = getNumArgs(command) + 1;
			char* histArgs[newLength];
			strcpy(historyArr, command.c_str());
			
			
			char** nxtArg = histArgs;
			char* cmmd = strtok(historyArr, " ");
			
			while(cmmd != NULL)
			{
				*nxtArg++ = cmmd;
				cmmd = strtok(NULL, " ");
			}
			*nxtArg = NULL;
			
			//recurse through this method again to process the new command
			return parseCommand(histArgs, newLength, history, previousTime);
			
		}
			
	}
	else if(strcmp(args[0], "cd") == 0)
	{
		int error = chdir(args[1]);
		if(error < 0)
			std::cout << "Error Changing Directories" << std::endl;
	}
	//if its not a custom command, run it through to execvp
	else
	{
		return execCommand(args);
	}
	
	//return this if it was a custom command (ptime, history, ^ #)
	return -1;
}

//this method finds and returns the number of arguments in the string
int getNumArgs(std::string s)
{
	//start at the beginning with no arguments
	int args = 0;
	int pos = 0;
	//keep going until theres no more spaces found, assign space position to pos
	while((pos = s.find(" ")) != -1)
	{
		//increase args, erase string up to the first space
		args++;
		s.erase(0, pos+1);
	}
	//if theres still another arg left after the last space was found add 1 more to args
	if(s.length() > 0)
		args++;
	return args;
}

//prints the history 
void printHistory(std::vector<std::string> history)
{
	std::cout << "-- Command History --" << std::endl;
	for(int i = 0; i < history.size(); i++)
		std::cout << i+1 << ": " << history.at(i) << std::endl;
}

//executes an execvp command and returns the time spent executing
double execCommand(char* args[])
{
	int PID = 0;
			
	PID = fork();
				
	//if parent process
	if(PID > 0)
	{
		auto start = std::chrono::system_clock::now();
		wait(NULL);
		auto stop = std::chrono::system_clock::now();
		
		std::chrono::duration<double> time = stop - start;
		return time.count();
		
	}
	//if child process
	else if(PID == 0)
	{
		execvp(args[0], args);
		
		//should only get here if it was an invalid command
		std::cout << "Invalid Command" << std::endl;
		exit(0);
	}
}


