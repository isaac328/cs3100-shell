#include<iostream>
#include<sstream>
#include<string>
#include<unistd.h>
#include<sys/wait.h>
#include<stdio.h>
#include<string.h>
#include<chrono>
#include<ctime>
#include<vector>
#include<iomanip>
#include<csignal>
#include<errno.h>


int getNumArgs(std::string s);
void printHistory(std::vector<std::string> history);
double execCommand(char* args[]);
double parseCommand(char* args[], int len, std::vector<std::string> &history, double &previousTime);
void signalHandler(int signum);
void printPrompt();
int numPipes(char* args[]);
void setPipeLocations(char* args[], int numPipes, int* locs);
void pipeCommand(char* args[], int len, int pipes);

